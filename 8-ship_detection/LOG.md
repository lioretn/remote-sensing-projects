# Log

This file is intended to record the development of our project.

## 2021-04-29
### Work done during the session
* Global presentation of the projects
* We've chosen our project
* We've created our github which took us some time as there were little problems

### Work planned for the next session
* Watch the "MIT - Introduction to Deep Learning" video
* Read "Introducing Scikit-Learn"
* Choose a machine learning method (idea : deep learning for now)
* Start thinking about the code


## 2021-05-10
### Work done during the session
* We've created (again) our github
* We've done our first little program which only show examples of images that we will have to treat
* We've prepared our presentation for the next session

### Work planned for the next session
* We must prepare ourself for the presentation
* Choose a machine learning method and start thinking about the code


## 2021-05-17
### Work done during the session
* Oral presentation
* Lesson about how to use git
* We've started to think about different methods (easier than deep learning)

### Work planned for the next session
* Start to implement these easiest methods
* Read the excell file


## 2021-05-20
### Work done during the session
* First methods implemented : it consists in calculating the average color of the image and comparing every pixel to this value, considering a gap, in order to detect ships (color different from water coloration). It works well for classical images (uniform water with a ship from a different color), but it is completly ineficient when there is a littoral or clouds or even a ship's wake.
* We tried to make a noise-reducing program in order to lower the error (pixels that aren't ships) but it didn't worked well...
* We don't have the right images in the excel (only the test set, we don't have the position of the ships) so we didn't really advanced on the interpretation of the excel data...

### Work planned for the next session
* We have to download a new dataset of image among the "big" one (train dataset) and to split it in two different one : for training and for testing.
* Then we will have to interpret the excell in order to test our first method
* We will implement the method of the gradient and, maybe, start to implement a machine learning method (more complex)


## 2021-05-25
### Work done during the session
* We've finished to implement the first method of average color
* We've implemented the gradient method for detecting contours
* We've coded a little program in order to create a new Excel including only the right images (the ones we use)
* We've done the code that allow us to generate a binary image locating the boats from the excell informations

### Work planned for the next session
* Dowload the entire dataset and split it in two sets : train and test
* We have to start to implement a machine learning method (named random forest)

## 2021-05-27
### Work done during the session
* We've finalised the train data shaping
* We've prepared a little dataset only composed of images relatively easy to treat
* We've changed different programs because we didn't understood well the excel

### Work planned for the next session
* Finish the encoding program
* Continue the implementation of the machine learnning method

## 2021-05-31
### Work done during the session
* We've implemented a first version of the machine learning method which wasn't really good...
* We've continued to develop the gradient method
* We've started to code a function that differentiate each boat from another and save the datas in the excel as required by Airbus

### Work planned for the next session
* Think about a new way to give the data to the machine learning method (it requires a specific form)
* With the gradient method, fullfil the interiors of the boats
* End the encoding algorithm

## 2021-06-31
### Work done during the session
* We've reduced the number of pixels on each train image (50% ship pixels, 50% non-ship pixels) in order to lighten the number of datas to compute : it gives far better results (even if it's still hard for it to differentiate boats from litoral, clouds...)
* We've tested different machine learning algorithms on the "easy_dataset" (100 easy images)
* We've tried to implement a deep learning method thanks to Tensorfow but rapidly forgot that idea (the difficiculty is to generate an adapted dataset)
* We didn't managed to fullfil the boat's contour with ship pixels in the gradient method, so we've stopped working on it for now.
* We started to code different unnoising methods for differentiate a ship from another element

### Work planned for the next session
* Find out the best machine learning method and test it on a bigger set
* Try other methods of unnoising for considering the spatial organization (density, symetry, orientations...)
* Try these unnoising methods on the results of the machine learning
