Concerning the encoding of the image in the format required by Airbus : file Encoding
	First, you must have a binary image (at least the first canal, the one checked, as to be binary (0 or 255)), then the encoding happens in
three steps :
		- the first encoding : thanks to the function encode_image, it gives you a list of the different ships in the image
(group of pixel), each ship is a list of Section objects (see the definition with the example). This first encoding is very important as the list
returned is indispensible for the different form filter as we will see later. This encode_image function call areLinked and formBoat functions.
		- the second encoding : you can pass the boats list returned by encode_image to the encode_boats function which only put
these data in the form that will be writen in the excel (a string).
		- the writing : by giving a list of the names of the images and the list returned by encode_boats to write_in_excel
function, an excel file will be created in the folder of your choice (path parameter)
	The last function of the file Encoding, the function image, only allows the creation of a numpy image (with colors for each ship) from
a list as returned by encode_image, it's only for ploting the images (it can also plot the results of the different filters).

Concerning the uses of the different form filters : file Form_Filter
	First, you must have the boats list returned by Encoding.encode_image.
	You must pass it to the boats_box function which generates boats around each boat (virtual one, you can plot it if you want but it isn't
needed, see the lower part of the color_average_Ioan_noise_reduction file.
	Then you must generate the images of each boat thanks to the boats_as_image function (it uses the results of the 2 previous functions)
	Finally, you can use the different form filters by calling it with the boats list returned by encode_image and the boxs list returned
by boats_box, it will return you a list as the boats list, that can be directly send to Encoding.encode_boats then Encoding.write_in_excel for 
saving the results. Or you can simply plot it with Encoding.image.
	There are 3 form filters, linked to the function density_filter, symetry_filter and orientation_filter (the last one calls the slice_list
function).

About Excell_Resizing : from train_ship_segmentation.csv it creates a smaller excel file with a set of image that you choosed, 

About Excell_Resizing_random : from train_ship_segmentation.csv it creates a smaller excel file with a random set of image, you dont need to give any image path

About machine_learning : from a excel file, it creates the features' vectors for the train by reading the excel file and the images. Then it trains some models from scikit learn and make some predictions et accuracy estimations on a given test set. You need to know the paths of the files.
