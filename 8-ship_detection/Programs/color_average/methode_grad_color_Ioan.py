from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

#test images 0f01e20db, 0a19c1ef7, 0f10acf24, 1a36da3c8, 00dc34840

nom_image = "1a36da3c8.jpg"

#%% creation of a binary image using the gradient method

#open image

image_ent = Image.open(nom_image,"r")
image1 = np.asarray(image_ent)

plt.imshow(image1)
plt.title("image d origine")
plt.figure()

image = np.zeros([768,768,1])

#here I transform the RGB image into a monocolor image using the norm

for k in range (768):                      
    for j in range(768):
        image[k,j,0] = np.sqrt(image1[k,j,0]**2+image1[k,j,1]**2+image1[k,j,2]**2)

#Here I create image using the gradient in 2 dimention 
#This alos us to visualize the variations of color and identify the boats outlines

imgrad = np.zeros([768,768,1])

for l in range (768):                      
    for m in range(768):
        imgrad[m,l,0] = np.sqrt((image[m,l,0]-image[m-1,l,0])**2+(image[m,l,0]-image[m,l-1,0])**2)

#Now I calculating the medium value of the gradian image

moy = 0
err = 30  #margin of error

for n in range (768):                      
    for o in range(768):
            moy = moy + imgrad[n,o,0]
moy = int(moy/(768*768))

#creation of a binary image depending of the medium value and the margin of error

imgradBin = np.copy(imgrad)

for p in range (768):
    for q in range(768):
        if (moy-err<=imgrad[p,q,0]<=moy+err):
            imgradBin[p,q,0] = 0
        else:
            imgradBin[p,q,0] = 255

#showing the gradient binary image

plt.imshow(imgradBin)
plt.title("image grad")
plt.figure()
    

#%% creation of a binary image base on the colors


from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

#opening the image

image_ent = Image.open(nom_image,"r")
image1 = np.asarray(image_ent)
image = np.copy(image1)

moyR = 0
moyG = 0
moyB = 0
err = 50   #margin of error

#Here I am calculating the medium value for eache pixel color (Red, Green and Blu)

for k in range (768):
    for i in range(768):
            moyR = moyR + image[k,i,0]
            moyG = moyG + image[k,i,1]
            moyB = moyB + image[k,i,2]
moyR = int(moyR/(768*768))
moyG = int(moyG/(768*768))     
moyB = int(moyB/(768*768))


#creation of a binary image depending of the medium value of each color (R,G and B) of and the margin of error

imageBIN = np.zeros([768,768,1])
imageBIN[:,:,0] = image[:,:,0]

for j in range (768):
    for l in range(768):
        if (moyR-err<=image[j,l,0]<=moyR+err and moyG-err<=image[j,l,1]<=moyG+err and moyB-err<=image[j,l,2]<=moyB+err):
            imageBIN[j,l,0] = 0
        else:
            imageBIN[j,l,0] = 255

#showing the color binary image

plt.imshow(imageBIN)
plt.title("image moy couleur")
plt.figure()


#%% Creation of two images which are a combination of the last two 

imagefinmoy = np.zeros([768,768,1])
imagefinprod = np.zeros([768,768,1])

for t in range (768):
    for y in range(768):
        imagefinmoy[t,y,0] = int((imageBIN[t,y,0]+imgradBin[t,y,0])/2)          #average of the two firts images
        imagefinprod[t,y,0] = int((imageBIN[t,y,0]*imgradBin[t,y,0])/255)       #normalized product of the two firts images

plt.imshow(imagefinmoy)
plt.title("image fin moy")
plt.figure()
plt.imshow(imagefinprod)
plt.title("image fin prod")

#%% Trying to creat a zone around the boats


X = 30
Y = 30
angle = np.linspace(0,2*np.pi,100)
x = (np.linspace(0,X,X+1))
y = (np.linspace(0,Y,Y+1))


imagefinprodrec = np.copy(imagefinprod)                                                                  #the idea is to applicate a sorrt of intercorelation between a recatangle and the image in order to find the boats position and orientaion      

for a in range(X,768-X):
    for b in range(Y,768-Y):
        if imagefinprodrec[a,b,0] == 255:
            Lsomme = []
            Ltheta = []
            for theta in angle :
                cos = np.cos(theta)
                sin = np.sin(theta)
                somme = 0
                for c in range (X-1):
                    for d in range (Y-1):
                        somme = somme + imagefinprodrec[int(a+c*cos-d*sin),int(b+c*sin+d*cos),0]
                Lsomme.append(somme)
                Ltheta.append(theta)
                somme_max = max(Lsomme)
                theta_max = Ltheta[Lsomme.index(somme_max)]
                cos_max = np.cos(theta_max)
                sin_max = np.sin(theta_max)
                for e in range (X-2):
                    for f in range (Y-2):
                        if imagefinprodrec[int(a+e*cos_max-f*sin),int(b+e*sin_max+f*cos),0] == 0:
                            imagefinprodrec[int(a+e*cos_max-f*sin),int(b+e*sin_max+f*cos),0] = 127
                        if imagefinprodrec[int(a+e*cos_max-f*sin),int(b+e*sin_max+f*cos),0] == 255:
                            imagefinprodrec[int(a+e*cos_max-f*sin),int(b+e*sin_max+f*cos),0] = 254

plt.figure()
plt.title("rectangles")
plt.imshow(imagefinprodrec)

                     
