from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import time

import sys
if "../" not in sys.path:
    sys.path.append("../")
    
import Encoding as e
import Form_Filter as ff

def color_average_noise_reduction(image_ent, err = 30, pre_noise_reduction = False): 
    ''' color_average_noise_reduction(image_ent, err = 50)
            Creation of a binary image giving the position of the ships on the image thanks to a method of
        comparison of the pixel values to the average value of the pixels in the image.
            This method works rather well for the easiest cases where there are no litoral, nor clouds or foam.
            
            Parameters :
                - image_ent : image to analyse
                - err : admited gap regarding the value of the pixel compary to the average
                
            Sortie :
                - imageSB : binary image with less noise
    '''

    image = np.copy(np.asarray(image_ent))
    moyR = 0 ; moyG = 0 ; moyB = 0
    
    print("Execution time :") # Let's show the time it takes to treat an image with this method
    t1 = time.process_time()
    
    # Computing the average color of the image
    for k in range (768):                      
        for i in range(768):
                moyR = moyR + image[k,i,0]
                moyG = moyG + image[k,i,1]
                moyB = moyB + image[k,i,2]
    moyR = int(moyR/(768*768))
    moyG = int(moyG/(768*768))     
    moyB = int(moyB/(768*768))
    
    t2 = time.process_time()
    print(" - average computing : ", t2 - t1, "s")
    
    # Creation of the binary image, regarding the distance of each pixel to the average color
    for j in range (768):
        for l in range(768):
            # That's water
            if (moyR-err<=image[j,l,0]<=moyR+err and moyG-err<=image[j,l,1]<=moyG+err and moyB-err<=image[j,l,2]<=moyB+err):
                image[j,l,0] = 0
                image[j,l,1] = 0
                image[j,l,2] = 0
            # That's a ship
            else:
                image[j,l,0] = 255
                image[j,l,1] = 255
                image[j,l,2] = 255
                
    t3 = time.process_time()
    print(" - binary image creation : ", t3 - t2, "s")
    
    '''# Noise reduction
    imageSB = np.copy(image)
    repetitionSB = 2 # Number of repetitions of the algorithms of noise reduction
    val = 3 # Precision criteria, for choosing between water and ship
    
    # We count the number of pixels "ship" in the neighborhood of the pixel we look at, if 
    # there's more "ship" pixels than val, we consider that it's effectively a ship
    for q in range(repetitionSB):
        for m in range(1,767):
            for n in range (1,767):
                if (image[m,n,0] != 0):
                    compteur = 0 
                    if (image[m+1,n,0] == 0) : compteur += 1
                    if (image[m-1,n,0] == 0) : compteur += 1
                    if (image[m,n+1,0] == 0) : compteur += 1
                    if (image[m,n-1,0] == 0) : compteur += 1
                    if (image[m+1,n+1,0] == 0) : compteur += 1
                    if (image[m-1,n-1,0] == 0) : compteur += 1
                    if (image[m-1,n+1,0] == 0) : compteur += 1
                    if (image[m+1,n-1,0] == 0) : compteur += 1
                    if (compteur >= val) : 
                        imageSB[m,n,0] = 0
                        imageSB[m,n,1] = 0
                        imageSB[m,n,2] = 0'''
                
    boats = e.encode_image(image)
    
    t4 = time.process_time()
    print(" - first unnoising + encoding : ", t4 - t3, "s")
    print(" - total : ", t4 - t1, "s")
    print()
    
    return boats, image

##### Here we test the different unoising functions #####
test_set = ["0ae37aa28.jpg", "0ae49bc36.jpg", "0ae67f3c6.jpg", "0af984fcd.jpg", "0b0892a75.jpg", "0bc0716c4.jpg",
            "0b8cc092d.jpg", "0b926376a.jpg", "0ba5e5a8a.jpg", "0b3fd8cee.jpg", "0b4894dc2.jpg", "0ba29cbcf.jpg",
            "0bb22ff9b.jpg", "0bba6cab0.jpg", "0bd931c24.jpg", "0bd57733e.jpg", "0bebd187e.jpg", "0bfc39ae9.jpg",
            "0c2f49144.jpg", "0c6b7bca1.jpg", "0cd642bc1.jpg", "0d2eb8a3b.jpg", "0d4704282.jpg", "0dae9b559.jpg"]
j = 0
for path in test_set[:1]:
    # By the color average method
    '''image_ent = Image.open("../../../../data/train/" + path)
    boats, image = color_average_noise_reduction(image_ent)
    image = Image.fromarray(image)
    image.save("bimage/" + path[:-3] + ".png")'''
    
    original = np.asarray(Image.open("../../../../data/train/" + path))
    
    # Loading directly the binary images
    image = np.asarray(Image.open("bimage/" + path[:-3] + ".png"))
    
    boats = e.encode_image(image)
    boxs =  ff.boats_box(boats)
    boats_images = ff.boats_as_image(boats, boxs)
    imageSB = e.image(boats)
    
    plt.figure()
    plt.imshow(imageSB)
    #plt.savefig("Binary_" + str(j) + ".png")
    
    '''boats = ff.symetry_filter(boats, boats_images)
    boxs = ff.boats_box(boats)
    boats_images = ff.boats_as_image(boats, boxs)
    imageSB = e.image(boats)'''
    
    boats = ff.orientation_filter(boats, boats_images)
    boxs = ff.boats_box(boats)
    boats_images = ff.boats_as_image(boats, boxs)
    imageSB = e.image(boats)
    
    plt.imshow(imageSB)
    '''for i in range(len(boxs)):
        box = boxs[i]
        plt.gca().add_patch(Rectangle(box[0], box[2], box[1], edgecolor='w', facecolor='none', linewidth=1.0))
        plt.text(box[0][0], box[0][1] - 10, str(i), color='w')'''
        
    #plt.savefig("Orientation_" + str(j) + "bis.png")
    j += 1
    
    plt.show()
    
    
    '''imageSB = e.image(boats)
    plt.imshow(imageSB)
    for box in boxs:
        plt.gca().add_patch(Rectangle(box[0], box[2], box[1], edgecolor='w', facecolor='none', linewidth=1.0))
    plt.show()'''
    
    '''# Here we try the Density filter
    boats = ff.density_filter(boats, boats_images)
    boxs = ff.boats_box(boats)
    boats_images = ff.boats_as_image(boats, boxs)
    imageSB = e.image(boats)
    
    plt.figure()
    plt.imshow(imageSB)
    for box in boxs:
        plt.gca().add_patch(Rectangle(box[0], box[2], box[1], edgecolor='w', facecolor='none', linewidth=1.0))
    plt.title("Density")
    #plt.savefig("density_filter/Density_" + path)
    plt.show()'''
   
    # Here we try the Symetry filter
    boats = ff.symetry_filter(boats, boats_images)
    boxs = ff.boats_box(boats)
    boats_images = ff.boats_as_image(boats, boxs)
    imageSB = e.image(boats) 
   
    plt.figure()
    plt.imshow(imageSB)
    for i in range(len(boxs)):
        box = boxs[i]
        plt.gca().add_patch(Rectangle(box[0], box[2], box[1], edgecolor='w', facecolor='none', linewidth=1.0))
        plt.text(box[0][0], box[0][1] - 10, str(i), color='w')
        #plt.subplot(len(boats)//5 + 1, 5, i+1)
        #plt.imshow(boats_images[i])
        plt.imshow(boats_images[i])
        plt.savefig("boat" + str(i) + ".png")
    plt.title("Symetry")
    plt.savefig("density + symetry/Symetry_" + path)
    plt.show()
    
    '''# Here we try the Orientation filter
    boats = ff.orientation_filter(boats, boats_images)
    boxs = ff.boats_box(boats)
    boats_images = ff.boats_as_image(boats, boxs)
    imageSB = e.image(boats) 
    
    plt.figure()
    plt.imshow(imageSB)
    for i in range(len(boxs)):
        box = boxs[i]
        plt.gca().add_patch(Rectangle(box[0], box[2], box[1], edgecolor='w', facecolor='none', linewidth=1.0))
        plt.text(box[0][0], box[0][1] - 10, str(i), color='w')
    plt.title("Orientation")
    plt.savefig("density + symetry + orientation/Orientation_" + path)
    plt.show()'''
    
'''image_ent = Image.open("../../../../data_easy/train/" + test_set[0])
boats = color_average_noise_reduction(image_ent)
boxs =  ff.boats_box(boats)
boats_images = ff.boats_as_image(boats, boxs)
imageSB = e.image(boats)
print("L1 = ", len(boats))

plt.imshow(imageSB)
for box in boxs:
    plt.gca().add_patch(Rectangle(box[0], box[2], box[1], edgecolor='w', facecolor='none', linewidth=1.0))

boats = ff.density_filter(boats, boats_images)
boxs = ff.boats_box(boats)
imageSB = e.image(boats)
print("L2 = ", len(boats))

plt.figure()
plt.imshow(imageSB)
for box in boxs:
    plt.gca().add_patch(Rectangle(box[0], box[2], box[1], edgecolor='w', facecolor='none', linewidth=1.0))

plt.savefig("Density_" + test_set[0])
    
plt.show()'''
