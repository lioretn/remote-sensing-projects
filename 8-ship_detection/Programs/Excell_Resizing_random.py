# -*- coding: utf-8 -*-

#############################################################################
# Update the excel  in order to consider only the images we use #
#############################################################################

import pandas as pd
import random


#Create a train set of random images

size = 1000
train_filename = "train_ship_segmentations_v2.csv"
training_data_path = ""
training_images_path = training_data_path                                #I put the images and the csv file in the same folder
training_data = pd.read_csv(training_data_path+train_filename)         #reading the csv file (replace cvs by xls if excel file)
print(training_data.shape)                                               #to check the shape of the data
print(training_data.head())                                              #display a few data to check it 
training_Image_list = list(training_data.ImageId.unique())           #List of the dataset image names
rdm_list = random.choices(training_Image_list, k=size)



from xlwt import Workbook # For writing in the excel

csv = open("train_ship_segmentations_v2.csv", 'r') # Original file

# Creation of the new excel
new_excell = Workbook()
sheet = new_excell.add_sheet("OCB")
    
csv_lines = csv.readlines()
# Header
csv_lines[0] = csv_lines[0].split(",")
sheet.write(0, 0, csv_lines[0][0]) # ImageID
sheet.write(0, 1, csv_lines[0][1][:-1]) # EncodedPixels
image_names = []

size = len(rdm_list) # Number of images in the considered dataset (test or train) = number of files in the folder
c_images = 0 ; c = 0 ; i = 1
# We get the names of the images and we add the corresponding line from the original excel lines to the new excel
for i in range(1,len(csv_lines)):
   csv_lines[i] = csv_lines[i].split(",")
   path = "../../../data/complete_train_dataset/"  + csv_lines[i][0] # The theorical path of the image (we can put test or train)
   # If the file exists, we add the coresponding line from the original excel to the new one
   if csv_lines[i][0] in rdm_list: 
       print("")
       with open(path, 'r') as file: # Trying opening the image
           sheet.write(c+1, 0, csv_lines[i][0])
           sheet.write(c+1, 1, csv_lines[i][1][:-1])
           if csv_lines[i][0] not in image_names:
               image_names.append(csv_lines[i][0])
               c_images += 1
           c += 1
           print("Avancement :", c_images/size*100, "%") # Pour voir où on en est...
   


# Close the original excel and save the new one               
csv.close()
new_excell.save("train_random_1000_ship_segmentations.xls")  # Save (we can put test or train)      
    