# -*- coding: utf-8 -*-

import numpy as np
from math import atan, cos, sin, pi, sqrt, atan

import matplotlib.pyplot as plt

##########################################################################################################################################
#####------------------------------------------------------UTILITARY FUNCTIONS-------------------------------------------------------#####
##########################################################################################################################################

def del_boats(boats, to_del):
    '''
        Function that deletes elements of the list boats according to the number of cases in to_del list.
        It returns a modified copy of the boats list
    '''
    count = 0 # We consider that the size of boats_copy get smaller at each suppression
    boats_copy = boats[:]
    for i in to_del: # to_del is composed of the cases to delete in boats list (the numbers)
        boats_copy.pop(i - count)
        count += 1
        
    return boats_copy


def boats_box(boats):
    '''
        This function returns the boxs that encadrates the different boats of the boats list.
        The boxs list that is returned is a tuple considering the position, the width and the height of the box, as required
    by the matplotlib.patches.Rectangle class.
    '''
    boxs = []
    for boat in boats: # For each boat, we create a box
        x = [] ; y = [] # The positions of the pixels
        for s in boat: # For each section (see class Section in Encoding.py), we update the pixels location
            x += [s.start_pos[0]] # The x value doesn't change in a section
            y += [s.start_pos[1], s.start_pos[1] + s.lenght] # We only consider the minimum and maximum values (faster)
        
        boxs.append(((min(y), min(x)), max(x) - min(x) + 1, max(y) - min(y) + 1)) # Corresponds to the matplotlib condtions for drawing rectangles
        
    return boxs


def boats_as_image(boats, boxs):
    '''
        Thanks to this function, we can transform the boats list (where each boat in a list of sections) to a list far easier
    to work on (where each boat is a binary image of the size of his box).
    '''
    boats_images = [] # The new list of image like boats
    for i in range(len(boats)):
        boat_image = np.zeros([boxs[i][1], boxs[i][2], 1]) # Image creation (can be shown thanks to imshow image)
        ymin, xmin = boxs[i][0] # The origin of the box
        for s in boats[i]: # For each section of the boat, we fullfil the corresponding image
            xs, ys = s.start_pos[0] - xmin, s.start_pos[1] - ymin # Change of coordinate system position
            for y in range(ys, ys + s.lenght): # The y value changes
                boat_image[xs, y, 0] = 255 # The xs value is constant in a section
        boats_images.append(boat_image)
    
    return boats_images


def slice_list(l, start, stop):
    '''
        Thif function is a utilitary for the orientation_filter.
        It returns the part of the l list between start and stop and the indices linked to these cases.
    '''
    lenght = len(l)
    
    if start > stop:
        if start >= lenght:
            return l[start%lenght:stop+1], [i for i in range(start%lenght, stop+1)]
        elif stop < -1:
            return l[start:stop+1], [i for i in range(start, len(l)+stop+1)]
        elif stop == -1:
            return l[start:], [i for i in range(start, len(l))]
        return l[start:] + l[:stop], [i for i in range(start, len(l))] + [i for i in range(stop)] # General situation
    
    else:
        if stop >= lenght:
            return l[start:] + l[:stop%lenght+1], [i for i in range(start, len(l))] + [i for i in range(stop%lenght+1)]
        elif start < 0:
            return l[start:] + l[:stop+1], [i for i in range(len(l)+start, len(l))] + [i for i in range(stop+1)]
        return l[start:stop+1], [i for i in range(start, stop+1)] # General stuation

##########################################################################################################################################
#####------------------------------------------------------------FILTERS-------------------------------------------------------------#####
##########################################################################################################################################

def density_filter(boats, boats_images, min_value = 5):
    '''
        A first method to sort the different "ships" located by the encode_image function. 
        This method works on the density of the different pile of pixels, the most dense one (that are superiors to a given 
    limit) are supposed to be ships. Indeed, the ships are generally denser than foam, clouds and littorals...
        Parameters :
            - boats : the list each amounts of pixels on the image, each one are lists of Sections objects (see Encoding.py)
            - boats_images : the same boats under image form (result of the boats_as_image function)
            - min_value : the minimal number of average sticked pixels to consider the amount of pixels as a ship
    '''
    to_del = []
    for i in range(len(boats_images)): # As always, we study each supposed boat
        boat = boats_images[i]
        average_density = 0 ; count = 0 # We calculate the average density of the pile of pixels
        size = boat.shape
        
        # We only consider the center of the image (easier and the image is big enough to consider that the bordures have little impact)
        for x in range(1, size[0] - 1):
            for y in range(1, size[1] - 1):
                if boat[x, y, 0] == 255:
                    average_density += (boat[x+1, y, 0] + boat[x+1, y+1, 0] + boat[x, y+1, 0] + boat[x, y-1, 0] + boat[x-1, y-1, 0] + boat[x-1, y, 0]) / 255
                    count += 1
                    
        average_density /= count
        if average_density < min_value: # The condition for considering that the "boat" is not really a ship
            to_del.append(i) # List of the indices of the cases that have to be deleted
        
    return del_boats(boats, to_del) # We delete these cases and return the new list


def symetry_filter(boats, boats_images, adm_error = 0.3, dec = 0.2):
    '''
        This second methods works on the symetries to find which amount of pixels are ships and which are not. Indeed, there
    are always symetries among ships. It tries on four different orientation of symetries (horizontal, vertical, diagonal
    descendent and diagonal ascendent), and on three position of the axis too.
        But, truth be told, it doesn't works very well... Generally the density filter is more efficient.
        Parameters :
            - boats : the list each amounts of pixels on the image, each one are lists of Sections objects (see Encoding.py)
            - boats_images : the same boats under image form (result of the boats_as_image function)
            - adm_error : admissible error on the symetries for considering an amount of pixels as a ship
            - dec : relative distance between the displaced axis
        Returns the newly sorted boats list (after a passage to the del_boats function)
    '''
    to_del = []
    for i in range(len(boats_images)):
        boat = boats_images[i]
        # errors = [Horizontal, Vertical, Diagonal1, Diagonal2] and [centered, displaced up/right, displaced down/left]
        errors = [[0, 0, 0] for j in range(4)] 
        count = 0 # For computing the average errors
        size = boat.shape
        
        # Positions of each angle of the box
        xA, yA = 0, size[1]-1 ; xB, yB = size[0]-1, size[1]-1 ; xC, yC = size[0]-1, 0 ; xD, yD = 0, 0
        xHo = (xA + xC + 1)/2 # The middle horizontal value (axis y = xHo as symetry axis)
        yVe = (yA + yC + 1)/2 # The middle vertical value
        a1, a2 = (yB - yD) / (xB - xD), (yC - yA) / (xC - xA) # Diagonal slopes (1 : descendent, 2 : ascendent)
        teta1, teta2 = atan(a1), pi/2 - atan(a2) # Angles forming by these diagonals with the x axis
        diag_lenght = sqrt(size[0]**2 + size[1]**2) # Lenght of the diagonal
        
        for x in range(size[0]):
            for y in range(size[1]):    
                # For each ship pixel, we check if his symetric by all the different axis is a ship pixel too, and we
                # increase the error in consequence
                if boat[x, y, 0] == 255:
                    ##### Horizontal symetries #####
                    # Centered axis
                    dHo = abs(x - xHo) # Distance to the axis (y = xHo)
                    sign = -1 # To reduce the quantity of lines : -1 if the point we study is to the right of the axis, 
                    # 1 otherwise
                    if x < xHo:
                        sign = 1
                    if boat[x + sign*int(2*dHo) - sign, y, 0] == 0: # If the symetric isn't a ship,
                        errors[0][0] += 1 # we increase the error linked to this symetry
                    
                    # Displaced axis
                    for eps in [-1, 1]: # -1 : axis displaced to the left, 1 : to the right
                        xHo_dis = (1 + eps*dec)*xHo # New position of the axis (y = xHo_bis)
                        dHo_dis = abs(x - xHo_dis) # Distance
                        sign = -1
                        if x < xHo_dis:
                            sign = 1
                        try: # We only consider the pixels that exist...
                            pos = x + sign*int(2*dHo_dis) - sign
                            if pos >= 0: # We want it positive because we don't want to "take the list in the other way"
                                if boat[pos, y, 0] == 0: # We check the symetric
                                    errors[0][eps] += 1
                        except:
                            pass
                      
                    ##### Vertical symetries #####
                    # It works exactly the same way as the horizontal symetries but with y rather than x
                    # Centered one
                    dVe = abs(y - yVe)
                    sign = -1
                    if y < yVe:  
                        sign = 1
                    if boat[x, y + sign*int(2*dVe) - sign, 0] == 0:
                        errors[1][0] += 1
                        
                    # Displaced ones
                    for eps in [-1, 1]:
                        yVe_dis = (1 + eps*dec)*yVe
                        dVe_dis = abs(y - yVe_dis)
                        sign = -1
                        if y < yVe_dis:
                            sign = 1
                        try:
                            pos = y + sign*int(2*dVe_dis) - sign
                            if pos >= 0:
                                if boat[x, pos, 0] == 0:
                                    errors[1][eps] += 1
                        except:
                            pass
                            
                    ##### Diagonal descendent symetries #####
                    # Centered one
                    # We change of coordinate system to the one centered in the up-left corner of the box and with the 
                    # descendent diagonal as X1 axis : (x, y) => (X1, Y1)
                    X1, Y1 = x*cos(teta1) + y*sin(teta1), -x*sin(teta1) + y*cos(teta1)
                    # We return in the normal coordinate system after having inversed Y1 : (X1, -Y1) => (x, y)
                    xp, yp = X1*cos(teta1) + Y1*sin(teta1), X1*sin(teta1) - Y1*cos(teta1)
                    try: # In order to dodge the errors related to the pixels "ejected" of the image because of symetries
                        if boat[int(xp), int(yp), 0] == 0 or boat[int(xp) + 1, int(yp) + 1] == 0: # Same principle as before
                            errors[2][0] += 1
                    except:
                        pass
                    
                    # Displaced ones
                    # The principle is the same but the diagonal are displaced here
                    for eps in [-1, 1]: # -1 : diagonal under the centered one, 1 : diagonal after the centered one
                        # The new Y1 coordinate is a bit more complex : Y1 - distance to the considered diagonal
                        Y1_dec = Y1 - dec*eps*diag_lenght
                        xp, yp = X1*cos(teta1) + Y1_dec*sin(teta1), X1*sin(teta1) - Y1_dec*cos(teta1) # (X1, Y1_dec) => (x, y)
                        try:
                            if boat[int(xp), int(yp), 0] == 0 or boat[int(xp) + 1, int(yp) + 1] == 0:
                                if Y1 < 0: # The positiveness of Y1 influences which diagonal is considered for increasing the error
                                    errors[2][eps] += 1   
                                else:
                                    errors[2][-eps] += 1   
                        except:
                            pass
                    
                    ##### Diagonal ascendent symetries #####
                    # Centered one
                    # The principle is the same as the next diagonal symetries, only there is already a shift here (xcenter)
                    xcenter = x - size[0] # We start to shift the coordinate system to the lower-left corner
                    X2, Y2 = xcenter*cos(teta2) + y*sin(teta2), -xcenter*sin(teta2) + y*cos(teta2) # New coordinates as before
                    # We return to the original coordinates after having inversed Y2
                    xp, yp = X2*cos(teta2) + Y2*sin(teta2) + size[0], X2*sin(teta2) - Y2*cos(teta2)
                    try:
                        if boat[int(xp), int(yp), 0] == 0 or boat[int(xp) + 1, int(yp) + 1, 0] == 0:
                            errors[3][0] += 1
                    except:
                        pass
                        
                    # Displaced ones
                    for eps in [-1, 1]:
                        Y2_dec = Y2 - dec*eps*diag_lenght # New Y2 coordinate (take into account the gap between the diagonals)
                        xp, yp = X2*cos(teta2) + Y2_dec*sin(teta2) + size[0], X2*sin(teta2) - Y2_dec*cos(teta2)
                        try:
                            if boat[int(xp), int(yp), 0] == 0 or boat[int(xp) + 1, int(yp) + 1] == 0:
                                if Y1 < 0:
                                    errors[3][eps] += 1   
                                else:
                                    errors[3][-eps] += 1   
                        except:
                            pass
                            
                    count += 1  
        
        errors = [errors[j][k]/count for j in range(4) for k in range(3)] # The errors of each symetry
        if min(errors) > adm_error: # The only thing we consider is the minimum of all these errors
            to_del.append(i) # If it is higher than the admissible error, it has to be deleted
            
    return del_boats(boats, to_del) # Returns the new list without the amounts of pixels not considered as ships by this function        
    
    
def orientation_filter(boats, boats_images, relation_max = 2, interval_teta = 30, angle_check = 45, relation_size = 2):
    '''
        Another filter that works on the direction of the pixels of the image compary to a center pixel (the average of every ship pixels),
    it then check different conditions (maximum, agnle, thickness) to determiner if the amount of pixel is a ship or not.
        Parameters :
            - boats : the list each amounts of pixels on the image, each one are lists of Sections objects (see Encoding.py)
            - boats_images : the same boats under image form (result of the boats_as_image function)
            - relation_max : maxmimum fraction value between the two maximums (M1 and M2)
            - interval_teta : interval of acceptance for the angle between the second maximum and the first maximum's axis
            - angle_check : caracterise the width of the side regions (angle interval around each main maximum)
            - relation_size : minimal value of the relation between the main maximums and the side maximums for considering the "boat" as
        a ship
    '''
    
    to_del = [] # The boats to delete
    interval_teta *= 2*pi/360 # Degrees -> Radiant
    angle_check *= 2*pi/360 # Degrees -> Radiant
    # Let's compute the average position of the pixels of each boat
    for i in range(len(boats_images)):
        a, b = 0, 0
        count = 0    
        boat = boats_images[i]
        size = boat.shape
        
        for x in range(size[0]):
            for y in range(size[1]):
                if boat[x, y, 0] == 255: # Ship pixel
                    a += x ; b += y
                    count += 1            
        a /= count ; b /= count # Average computing
        
        # Let's calculate the angles between each pixel and the average pixel
        # We will categorise these pixels in 64 categories (angle intervals)
        angles_amount = [0 for j in range(64)] # Number of pixels in each interval        
        for x in range(size[0]):
            for y in range(size[1]):
                if boat[x, y, 0] == 255: # Ship pixel
                    teta = 0
                    # Calculating the angle between the (x, y) pixel and the (a, b) average pixel of the boat
                    if x > a:
                        teta = atan((y-b) / (x-a))
                    elif  x < a:
                        teta = pi + atan((y-b) / (x-a))
                    elif x == a and y > b:
                        teta = pi/2
                    elif x == a and y < b:
                        teta = 3*pi/2
                    # else : teta = 0
                    
                    pos = int(teta*63/(2*pi)) # Angle -> Position in the angles and angles_amount lists
                    angles_amount[pos] += 1 # +1 pixel to this interval of angles
        
        # We now get to the heart of the matter, we have to decide whether this amount of pixel is really a ship or not
        # Let's tak about the global strategy :
        #   - we start by finding a global maximum of angles_amount
        #   - we then look for the global maximum of the other hemisphere (well visible in polar view)
        #   - a first ship condition is placed on the size difference between these two maximums
        #   - we compute the angle between the second maximum and the axis of the first one, another condition is place on it
        #   - we finally check that the width is far thiner than the lenght (form of a ship) by looking at the maximum of angles_amount 
        # in the side zones
        M1 = angles_amount.index(max(angles_amount)) # Position of the maximum
        hem1, hem2 = int(M1 - 63/4), int(M1 + 63/4) # Borders of the hemisphere which include M1
        # We call the slice_list function that generates a list of the values of the other hemisphere (with there indices in angles_amount)
        sl = slice_list(angles_amount, hem2, hem1)
        M2 = sl[1][sl[0].index(max(sl[0]))] # The find the position of the maximum of this second hemisphere
        amount_M1, amount_M2 = angles_amount[M1], angles_amount[M2] # The values of these two maximums
        
        # First "no-ship" condition : a maximum in far smaller than the other
        if amount_M1/amount_M2 > relation_max:
            to_del.append(i)
        else:
            teta = pi*(abs(M2 - M1)*2/63 - 1) # The angle between M2 and the axis passing by M1 and the center
            # Second "no-ship" condition : this angle is too high, meaning that the ship is forming a strange angle...
            if teta < -interval_teta or teta > interval_teta:
                to_del.append(i)
            else:
                # We now look at the side regions (perpendicular to the main axis)
                # These regions are delimited by reg1 and reg2
                reg1, reg2 = int(M1 + 63*angle_check/(2*pi)), int(M2 - 63*angle_check/(2*pi))
                sl = slice_list(angles_amount, reg1, reg2)
                M3 = sl[1][sl[0].index(max(sl[0]))] # The position of the maximum of this first side region
                amount_M3 = angles_amount[M3] # The value of the maximum of this first side region
                
                reg1, reg2 = int(M2 + 63*angle_check/(2*pi)), int(M1 - 63*angle_check/(2*pi))
                sl = slice_list(angles_amount, reg1, reg2)
                M4 = sl[1][sl[0].index(max(sl[0]))] # Maximum if the other side region
                amount_M4 = angles_amount[M4]
                
                # The last "no-ship" condition : the relation between the lenght and the width is too high (not the form of a boat)
                if min(amount_M1, amount_M2)/max(amount_M3, amount_M4) < relation_size:
                    to_del.append(i)
         
        # Let's show angles_amount in polar coordinates
        #plt.figure()
        #angles = np.linspace(0, 2*pi, 64) 
        #plt.polar(angles, angles_amount)
        #M = [angles_amount[M1], 0, angles_amount[M2]] ; M_pos = [M1*2*pi/63, 0, M2*2*pi/63] # In order to visualise the maximums M1 and M2
        #plt.polar(M_pos, M)
            
    plt.show()
    
    return del_boats(boats, to_del)