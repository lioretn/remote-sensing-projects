# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

from joblib import load
from PIL import Image
import imageio
import pandas as pd

import Encoding as e
import Form_Filter as ff

import time

# Function given by Airbus for reading the excel and creating the binary image associated
# Copy from machine_learning.py (see there for more explications)
def Binary_Image(image_name, data, filename, data_path):                                             
    bimage = np.zeros((768,768))
    data2 = pd.read_excel(data_path+filename, index_col='ImageId')              
    if type(data2['EncodedPixels'][image_name]) == float : return bimage        
    data_image = data[data['ImageId']==image_name]                               
    for index in data_image.index :                                             
        rle = [int(i) for i in data_image['EncodedPixels'][index].split()]      
        starts = list(rle[0:-1:2])                                              
        lengths = list(rle[1::2])                                               
        n = len(starts)                                         
        for k in range(n):
            bimage[starts[k] % 768 : starts[k] % 768 + lengths[k] , starts[k] // 768  ] = 1
    return bimage
    

def comparison(excel_image, pred_image):
    '''
        Function that compares the predicted image (result from machine learning + post-processing) to the
    true image using the criteria given by Airbus.
    '''
    AiB = 0 ; AuB = 0 # The 2 thing to calculate according to Airbus criteria (union and intersection)
    for x in range(768):
        for y in range(768):
            if excel_image[x, y] == 1 and [pred_image[x, y, i] for i in range(3)] != [0, 0, 0]:
                AiB += 1 # Intersection = and
            if excel_image[x, y] == 1 or [pred_image[x, y, i] for i in range(3)] != [0, 0, 0]:
                AuB += 1 # Union = or
    
    if AuB != 0:        
        # The error on this image (often 1 when there is nothing on both image or 0 when a false ship appears
        # for example (clouds, littoral...))
        return AiB / AuB
    return 1.0


# Function copied from the machine_learning.py file, see this file for more informations
def prediction(model, image_path, image_name):           
    X = np.zeros((768*768,3))
    f = imageio.imread(image_path+image_name)                                       
    for j in range(768) :
        for i in range(768) :
            X[768*j+i,0] = f[i,j,0]                         
            X[768*j+i,1] = f[i,j,1]
            X[768*j+i,2] = f[i,j,2]
    y = model.predict(X)
    predicted_image = np.zeros((768, 768, 3))
    for i in range(768*768 ):
        color = int(y[i]*255)
        predicted_image[ i%768, i//768, :3] = [color, color, color]
    return predicted_image.astype(np.uint8)

t1 = time.process_time()

score = 0 # The average score of a dataset

excel_path = "test_1000_ship_segmentations.xls"

names = [] # Names of the images of a dataset
test_excel = pd.read_excel(excel_path) # Here are noted the name and ships of some images
for i in test_excel.index:
    name = test_excel['ImageId'][i] # Name of the image
    if name not in names:
        names.append(name)
        
model_path = "machine_learning/"
model_name =  "model_SGDClassifier_random_1000.joblib" 
model = load(model_path+model_name)

image_path = "../../../data/complete_train_dataset/"

start, stop = 0, 1000 # Interval of calculation
advancement = 0
for name in names[start:stop]:
    post_process = False # If he program succeeded in applying the post-processing on the encoded image
    
    original_image = Image.open(image_path + name)
    image = prediction(model, image_path, name) # Results from the machine learning
    #plt.imshow(original_image)
    #plt.title("Original")
    
    # Encoding
    boats = e.encode_image(image) # First treatment and starting to encode in the right format for Airbus
    #boxs = ff.boats_box(boats) # Boxs that includes each pixel group
    #boats_images = ff.boats_as_image(boats, boxs) # Images of these pixel groups (for post-treatment)
    image = e.image(boats) # Image form for plotting
    '''plt.figure()
    plt.imshow(image)
    plt.title("ML")'''
    
    '''try:
        # Density filter
        boats = ff.density_filter(boats, boats_images)
        boxs = ff.boats_box(boats)
        boats_images = ff.boats_as_image(boats, boxs)
        
        # Orientation filter
        boats = ff.orientation_filter(boats, boats_images)
        boxs = ff.boats_box(boats)
        boats_images = ff.boats_as_image(boats, boxs)
        image = e.image(boats)
        
        plt.figure()
        plt.imshow(image)
        plt.title("ML + D + O")
        
        post_process = True
        
    except:
        pass
    
    if not post_process: # Not in the except because we want all or nothing for the post-treatment
        image = np.zeros((768, 768, 3)) # We prefer having an empty image (more likely)
        plt.subplot(1, 2, 2)
        plt.imshow(image)
        plt.title("ML")'''
        
    excel_image = Binary_Image(name, test_excel, excel_path, "")
    '''plt.figure()
    plt.imshow(excel_image)
    plt.title("Image from excel")'''
    
    score += comparison(excel_image, image) # Score sur cette image
    print(comparison(excel_image, image))
    
    advancement += 1
    print("Avancement : ", advancement/(stop - start)*100, "%")
    
    #plt.show()
    
score /= (stop - start) # Score moyen
print("Score sur un set de ", stop - start, " images :", score)

t2 = time.process_time() # Temps d'éxecution
print("Temps total : ", t2 - t1, "s")
    