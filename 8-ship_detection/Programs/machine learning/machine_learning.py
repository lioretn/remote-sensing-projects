## Ship detection ##


#WARNING : to run this code, you will need the excel file, the set of images and you will need to know the path to use them.
#To make is simpler : put all the files in the same folder and replace the paths in the folowing code by ""
#To avoid downloading the 30 GiB of images, you can download a few images and then create the excel file associated with Excell_Resizing.py
#To make it enven simpler : see the demo jupiter note book. We Took a few images and created the excel file, you just have to run it. The model used will be the model loaded. We calculated it on a 1000 images datda set.

#%% Importations


import numpy as np 
import matplotlib.pyplot as plt
import matplotlib.image as img
import pandas as pd
import imageio
import time
import random

from joblib import dump, load                             #To save and load our trained machine learning models

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import validation_curve
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import learning_curve
from sklearn.experimental import enable_hist_gradient_boosting
from sklearn.ensemble import HistGradientBoostingClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.svm import SVC
from sklearn.ensemble import AdaBoostClassifier



#%% Reading the training data

#The goal : create a machine learning model with input = pixel RGB vales, output = 1 (boat-pixel) or 0 (not a boat-pixel)
#This model on rely on the pixel color, it do no consider the spatial coherence

#Starting with the training set
#The features X are the pixels of each images
#For this supervised machine learning, the labels y will be deduce with the csv file
#First, let us format the data to be usable 

train_filename = "train_random_1000_ship_segmentations.xls"

training_data_path = ""

training_images_path = "D:/train_v2/"        

training_data = pd.read_excel(training_data_path+train_filename)         #reading the csv file (replace cvs by xls if excel file)
print(training_data.shape)                                               #to check the shape of the data
print(training_data.head())                                              #display a few data to check it 


#%% Ships / No Ships repartition

plt.bar(['Ships', 'No Ships'], 
    [len(training_data[~training_data.EncodedPixels.isna()].ImageId.unique()),
    len(training_data[training_data.EncodedPixels.isna()].ImageId.unique())]);
plt.ylabel('Number of Images');
plt.show()


#%% Binary image generation


#Airbus code : pairs of values that contain a start position and a run length. E.g. '1 3' implies starting at pixel 1 and running a total of 3 pixels in the column direction


def Binary_Image(image_name, data, filename, data_path):                                             #Input = image name + data name + filname + path, output = binary image (boat pixels = 1, no boat pixels = 0) using the 'EncodedPixels' data
    bimage = np.zeros((768,768))
    data2 = pd.read_excel(data_path+filename, index_col='ImageId')              #reading the csv file, using image names as index
    if type(data2['EncodedPixels'][image_name]) == float : return bimage        #no boat on the image, 
    data_image = data[data['ImageId']==image_name]                              #data_image group the data (rows) linked to the same image name 
    for index in data_image.index :                                             #for loop on the data from data_image to concider each boat (one line ~ one boat)
        rle = [int(i) for i in data_image['EncodedPixels'][index].split()]      #turn the encoded info in a list of split integers
        starts = list(rle[0:-1:2])                                              #list of the starts
        lengths = list(rle[1::2])                                               #list of the lengths
        n = len(starts)                                         
        for k in range(n):
            bimage[starts[k] % 768 : starts[k] % 768 + lengths[k] , starts[k] // 768  ] = 1       #The coordinate of the start k are : (starts[k] % 768, starts[k] // 768)
    return bimage

#Examples

plt.figure("1")
plt.subplot(1,2,1)
plt.imshow(Binary_Image('0ae37aa28.jpg', training_data, train_filename, training_data_path))       #2 boats
plt.subplot(1,2,2)
plt.imshow(img.imread("D:/train_v2/0ae37aa28.jpg"))


plt.figure("2")

plt.subplot(1,2,1)
plt.imshow(Binary_Image('0ae271a03.jpg', training_data, train_filename, training_data_path))       #no boat
plt.subplot(1,2,2)
plt.imshow(img.imread("D:/train_v2/0ae271a03.jpg"))

plt.figure("3")

plt.subplot(1,2,1)
plt.imshow(Binary_Image('0b8ce2b47.jpg', training_data, train_filename, training_data_path))       #8 boats
plt.subplot(1,2,2)
plt.imshow(img.imread("D:/train_v2/0b8ce2b47.jpg"))


plt.figure("4")

plt.subplot(1,2,1)
plt.imshow(Binary_Image('0ae49bc36.jpg', training_data, train_filename, training_data_path))       #2 boats but only one according to the airbus training 
plt.subplot(1,2,2)
plt.imshow(img.imread("D:/train_v2/0ae49bc36.jpg"))

#%% Notes about formatting  the features and label data 

#The goal is to formate the features and label data so they can be used by the scikit learn modules
#X is an array, each column is associated to R, G or B values of the pixels of the dataset images
#The elements of y are 0 or 1
#Number of lignes = total number of pixels
#The images dimension is 768*768

#The probleme of this methode is that X and y requier lots of memory (hundreds of GiB for more than 10 000 images)
#Plus, most of the pixel are no-boat pixels (about 99%) so the model learning won't be balanced
#Plus, the training time is very high, so it is impossible to optimize the hyperparameters
#For a better model, lighter data usage and shorter training time, we will use train data with a 50-50 propotion by randomly ignore non-boat pixels

#%% First methode to formatting  X and y (not the one we will use)

training_Image_list = list(training_data.ImageId.unique())           #List of the dataset image names
data_size = len(training_Image_list)

def get_X(Image_list, images_path):
    c = 0
    data_size = len(Image_list)                        #number of image considered, should be len(Image_list) but the algorithm require too much space
    X = np.zeros((data_size*768*768,3))                #features
    for k in range(data_size) :
        image_name = Image_list[k]
        f = imageio.imread(images_path+image_name)                     #read image, f is like (ligne,column,color)                        
        for j in range(768) :
            for i in range(768) :
                X[c+768*j+i,0], X[c+768*j+i,1], X[c+768*j+i,2] = f[i,j,0], f[i,j,1], f[i,j,2]       #concatenation for red values in the first X column
        c = c + 768**2
    return X
        

def get_y(data, data_path, filename ):
    c = 0
    Image_list = list(data.ImageId.unique())           #List of the dataset image names
    data_size = len(Image_list)                        #number of image considered, should be len(Image_list) but the algorithm require too much space
    y = np.zeros((data_size*768*768,))                 #labels
    for k in range(data_size) :
        image_name = Image_list[k]
        bimage = Binary_Image(image_name, data, filename, data_path)                        
        for j in range(768) :
            for i in range(768) :
                y[c+768*j+i] = bimage[i,j]
        c = c + 768**2
    return y


def get_X_and_y(images_path, data, data_path, filename ):
    c = 0
    Image_list = list(data.ImageId.unique())
    data_size = len(Image_list)                        
    X = np.zeros((data_size*768*768,3))                
    y = np.zeros((data_size*768*768,))                 
    for k in range(data_size) :
        image_name = Image_list[k]
        f = imageio.imread(images_path+image_name)                     
        bimage = Binary_Image(image_name, data, filename, data_path)                        
        for j in range(768) :
            for i in range(768) :
                X[c+768*j+i,0], X[c+768*j+i,1], X[c+768*j+i,2] = f[i,j,0], f[i,j,1], f[i,j,2]                   
                y[c+768*j+i] = bimage[i,j]
        c = c + 768**2
    return X , y

t1 = time.process_time()

# print("Calculating X...")
# X = get_X(training_Image_list, training_images_path)
# print("--> Done")
# print("Calculating y...")
# y = get_y(training_data, training_data_path, train_filename )
# print("--> Done")
# print(time.process_time()-t1)

# print("Calculating X and y...")
# t1 = time.process_time()
# X , y = get_X_and_y( training_images_path, training_data, training_data_path, train_filename)
# print("--> Done")
# print(time.process_time()-t1)
# print("Proportion of boats on the images", 100*sum(y)/(data_size*768*768),"%")


#%% Alternative to reduce the amount of data and increase the proportion of boat-pixels

#First we evaluate the proportion p of boat pixels
#Then we follow the same procedure to get X and y, but the no-boat pixels have a chance p of being considered

# Proportion p

def boat_probability(data, data_path, filename):
    c = 0                                              #boat-pixel counter 
    Image_list = list(data.ImageId.unique())           #List of the dataset image names
    data_size = len(Image_list)                        #number of image considered, should be len(Image_list) but the algorithm require too much space
    for k in range(data_size) :
        image_name = Image_list[k]
        bimage = Binary_Image(image_name, data, filename, data_path)                        
        for j in range(768) :
            for i in range(768) :
                c = c+bimage[i,j]                      
    return c/(data_size*768*768)


# Formatting  X and y

def get_X_and_y_light(images_path, data, data_path, filename, p ):
    Image_list = list(data.ImageId.unique())
    data_size = len(Image_list)                                                      #number of image considered, should be len(Image_list) but the algorithm require too much space
    R, G, B, y = [],[],[],[]                                                         #a list for each color, a list for the labels
    for k in range(data_size) :
        image_name = Image_list[k]
        f = imageio.imread(images_path+image_name)                                   #read image, f is like (ligne,column,color)    
        bimage = Binary_Image(image_name, data, filename, data_path)    
        for j in range(768) :
            for i in range(768) :
                b = bimage[i,j]
                if (b == 1 or (b == 0 and (random.random()<p)) ):                    #if this is a boat-pixel we consider it, if this is a no-boat pixel, the probability that it will be considered is equal to p (the proportion of boat-pixels)
                    R.append(f[i,j,0])                                               #We use lists because they can dynamically, arrays cannot so it would takes very long too use np.append (the array is copied every loop)
                    G.append(f[i,j,1])
                    B.append(f[i,j,2])
                    y.append(b)
    return np.column_stack((R,G,B)) , np.array(y)                                    #stack of red, green and blue --> features, y --> labels


t1 = time.process_time()
print("Calculating p...")
p = boat_probability(training_data, training_data_path, train_filename)
print("--> Done")
print("Proportion of boats on the images", 100*p,"%")

print("Calculing X and y (light version)...")
X , y = get_X_and_y_light(training_images_path, training_data, training_data_path, train_filename, p)
print("--> Done")
print("Proportion of boats in the features", 100*sum(y)/(np.shape(y)[0]),"%")
print("Calculated in : ",time.process_time()-t1,"s")
   
#%% Notes about machine learning

#To evaluate the model preformance, we split of the data set : the train set and the test set. So we can evaluate the model on unseen data
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2)
#But we take X_train = X because we will evalute each model accuracy on our random test set
X_train = X
y_train = y

#To have a good model, we must normalize the data through a tranformer (we will use the MinMax normalisation) before using them in the estimator
#To make it simple, we use "pipline" so the model normalize the data before using the estimator

#To get the best precision and the balance between over-fitting and under-fitting, we must tune the hyperparameters   
#Then we take a third part from the data set. To make sure we have the best model, we valide it on different split : this is called "cross validation"
#In scikit learn we can use cross_val_score to part the set and evaluate the score pn each
#We can also use validation_curve to see more easily which parameter is the best, but only one a the time
#To test combinaisons of hyperparameters, we use GridSearchCV

#Then, the learning curve allow us to know if the model would get better with a more datas


#%% 1) Gradient Boosting Classifier

#Optimised Model 
model1 = make_pipeline(MinMaxScaler(), GradientBoostingClassifier(loss='deviance',n_estimators=100 ,verbose=3)) #not the optimised model found but seems better on classic examples
model_name = 'model_gradientboostingclassifier_random_1000.joblib'   #must have '.joblib' as extension

#Training 
t1 = time.process_time()
model1.fit(X_train, y_train)
tt1=time.process_time()-t1
print("Model trained in",tt1)
dump(model1, model_name )  #Save the model
a=model1.score(X_test,y_test)
print("Its score is :",a)

#Hyperparameters optimisation (it can take lots of time to optimize)

# #Cross validation with validation_curve --> to find the best value for one parameter
# model1 = make_pipeline(MinMaxScaler(), GradientBoostingClassifier(verbose=3))
# n=[1,10,25,50,100]
# train_score, val_score = validation_curve(model1, X_train, y_train,'gradientboostingclassifier__n_estimators', n, cv=4 )
# plt.plot(n,val_score.mean(axis=1), label='validation')
# plt.plot(n,train_score.mean(axis=1), label='train')
# plt.legend()
# #With this machine learning algorithm, the higher n_estimators, the better results

# #Optimisation with Cross validation and GridSearchCV 
# parameters = {
#               'gradientboostingclassifier__learning_rate': [0.01,0.1,1],
#               'gradientboostingclassifier__n_estimators': [100],   # higher values will lead to better results 
#               }
# grid = GridSearchCV(model1, parameters, cv = 4)
# grid.fit(X_train, y_train)
# print(grid.best_score_)
# print(grid.best_params_)
# model1 = grid.best_estimator_
# a=model1.score(X_test, y_test)
# print(a)
# # Results : {'gradientboostingclassifier__learning_rate': 1}


# #Learning curve
# N, train_score2, val_score2 = learning_curve(model1, X_train, y_train, train_sizes = np.linspace(0.2,1.0,5))
# print(N)
# plt.figure('GBC_learning_cruve')
# plt.plot(N, train_score2.mean(axis=1), label='train')
# plt.plot(N, val_score2.mean(axis=1), label='validation')
# plt.xlabel('train_sizes')
# plt.legend()
# Results : 

#%% 2) Random Forest Classifier --> not good for samples > 100k

#Model and choice of the hyperparametres

model2 = make_pipeline(MinMaxScaler(),RandomForestClassifier())    

#Training 
t1 = time.process_time()
model2.fit(X_train, y_train)
model_name = 'model_randomforestclassifier_random_1000.joblib'
tt2=time.process_time()-t1
print("Model trained in",tt2)
dump(model2, model_name ) 
print('Model saved')
a = model2.score(X_test,y_test)
print("Its score is :",a)


# #Optimisation : we will take the defaults values, we already know that this model will not perform well because the samples are too numerous


#%% 3) K-Nearest Neighbors Classifier --> not good for samples > 100k

#Optimised model 


model3 = make_pipeline(MinMaxScaler(), KNeighborsClassifier(n_neighbors=17))

#Training 
t1 = time.process_time()
model3.fit(X_train, y_train)      #train the model
model_name =  'model_NearestNeighborsClassifier_random_1000.joblib'
tt3=time.process_time()-t1
print("Model trained in",tt3)
dump(model3, model_name) 
print('Model saved')
a = model3.score(X_test,y_test)
print("Its score is :",a)

#Hyperparameters optimisation (it can take lots of time to optimize)

# #Cross validation with validation_curve --> to find the best value for one parameter
# model3 = KNeighborsClassifier()
# k = np.arange(1,50)
# train_score, val_score = validation_curve(model3, X_train, y_train,'n_neighbors', k, cv=4 )
# plt.plot(k,val_score.mean(axis=1), label='validation')
# plt.plot(k,train_score.mean(axis=1), label='train')
# plt.legend()
# print("Model trained in",time.process_time()-t1,)
# #The maximum is reached for k = 23


# #Cross validation with GridSearchCV
# param_grid = {
#     'kneighborsclassifier__n_neighbors' : np.arange(1,50), 
#     'kneighborsclassifier__metric' : ['euclidean','manhattan']}  #Dictionary of the hyperparameters to optimize
# grid = GridSearchCV(model3,param_grid,cv=4)
# grid.fit(X_train, y_train)
# print(grid.best_score_)
# print(grid.best_params_)
# model3 = grid.best_estimator_
# a=model3.score(X_test, y_test)
# print(a)
# #the metric 'eucliean' (default) is better than manhattan, and the best k is 27 (validation scores very close to k=23 scores)


# #Learning curve
# N, train_score2, val_score2 = learning_curve(model3, X_train, y_train, train_sizes = np.linspace(0.2,1.0,5))
# print(N)
# plt.plot(N, train_score2.mean(axis=1), label='train')
# plt.plot(N, val_score2.mean(axis=1), label='validation')
# plt.xlabel('train_sizes')
# plt.legend()

#%% 4) Hist Gradient Boosting Classifier

#This estimator is much faster than GradientBoostingClassifier for big datasets (samples > 10k)
#But seems worse on classic examples

#Optimised Model 
model4 = make_pipeline(MinMaxScaler(), HistGradientBoostingClassifier(max_iter=1500,max_depth=50)) #not the optimised model found
model_name = 'model_Histgradientboostingclassifier_random_1000.joblib'   #must have '.joblib' as extension

#Training 
t1 = time.process_time()
model4.fit(X_train, y_train)
tt4=time.process_time()-t1
print("Model trained in",tt4)
dump(model4, model_name )  #Save the model
print("Model saved")
a=model4.score(X_test,y_test)
print("Its score is :",a)


# #Optimisation
# parameters = {
#  'histgradientboostingclassifier__max_iter': [1000,1200,1500],
#  'histgradientboostingclassifier__max_depth' : [25, 50, 75],
#  }
# #instantiate the gridsearch
# grid = GridSearchCV(model4, parameters, 
#  cv=5, scoring='f1_micro',
#  verbose=2, refit=True)
# #fit on the grid 
# grid.fit(X_train, y_train)
# print(grid.best_score_)
# print(grid.best_params_)
# model4 = grid.best_estimator_
# a=model4.score(X_test, y_test)
# print(a)


#%% 5) SGDClassifier --> seems good + fast

model5 = make_pipeline(MinMaxScaler(),SGDClassifier())
model_name = 'model_SGDClassifier_random_1000.joblib'   #must have '.joblib' as extension

#Training 
t1 = time.process_time()
model5.fit(X_train, y_train)
tt5=time.process_time()-t1
print("Model trained in",tt5)
dump(model5, model_name )  #Save the model
print("Model saved")
a=model5.score(X_test,y_test)
print("Its score is :",a)

#%% 6) Ada Boost Classifier --> good


model6 = make_pipeline(MinMaxScaler(),AdaBoostClassifier())
model_name = 'model_AdaBoostClassifier_random_1000.joblib'   #must have '.joblib' as extension

#Training 
t1 = time.process_time()
model6.fit(X_train, y_train)
tt6=time.process_time()-t1
print("Model trained in",tt6)
dump(model6, model_name )  #Save the model
print("Model saved")
a=model6.score(X_test,y_test)
print("Its score is :",a)

#Hyperparameters optimisation (it can take lots of time to optimize)


# param_grid = {"adaboostclassifier__criterion" : ["gini", "entropy"],
#               "adaboostclassifier__splitter" :   ["best", "random"],
#               "adaboostclassifier__n_estimators": [1, 2]
#              }


#%% Predictions on 100 random images to evaluate the accuracy of a model

test_filename = "test_100_ship_segmentations.xls"
test_data_path = "../../../../data_projet_ship_detection/"
test_data = pd.read_excel(test_data_path+test_filename)             #reading the csv file (replace xls by cvs if excel file)
test_images_path = "D:/train_v2/"
print(test_data.shape)                                              #to check the shape of the data
print(test_data.head())                                             #display a few data to check it 

test_Image_list = list(test_data.ImageId.unique())                  #List of the dataset image names


print("Calculating X_test...")
X_test = get_X(test_Image_list, test_images_path)
print("--> Done")
print("Calculating y_test...")
y_test = get_y(test_data, test_data_path, test_filename)
print("--> Done")


# Testing a model on the test set

# #load model
# model_path = "../../../../data_projet_ship_detection/models/"
# model_name =  'model_SGDClassifier_random_1000.joblib'
# model = load(model_path+model_name)

model = model1

t1 = time.process_time()
print("Calculating the predictions...")
y_predicted = model.predict(X_test)
print("--> Done")
tp1=time.process_time()-t1
print("Calculated in",tp1)


print("Calculating the accuracy of the model on the test data set...")
accuracy = 0
test_data_size = np.shape(y_test)[0] 
for k in range(test_data_size):
    if y_test[k]==y_predicted[k] : accuracy += 1
    
accuracy1 = 100*accuracy/test_data_size

print("--> Done")
print("The caccuraty is : ",accuracy1,"%")

#%%2

model = model2


t1 = time.process_time()
print("Calculating the predictions...")
y_predicted = model.predict(X_test)
print("--> Done")
tp2=time.process_time()-t1
print("Calculated in",tp2)

print("Calculating the accuracy of the model on the test data set...")
accuracy = 0
test_data_size = np.shape(y_test)[0] 
for k in range(test_data_size):
    if y_test[k]==y_predicted[k] : accuracy += 1
    
accuracy2 = 100*accuracy/test_data_size

print("--> Done")
print("The caccuraty is : ",accuracy2,"%")

#%%3

model = model3


t1 = time.process_time()
print("Calculating the predictions...")
y_predicted = model.predict(X_test)
print("--> Done")
tp3=time.process_time()-t1
print("Calculated in",tp3)

print("Calculating the accuracy of the model on the test data set...")
accuracy = 0
test_data_size = np.shape(y_test)[0] 
for k in range(test_data_size):
    if y_test[k]==y_predicted[k] : accuracy += 1
    
accuracy3 = 100*accuracy/test_data_size

print("--> Done")
print("The caccuraty is : ",accuracy3,"%")


#%%4

model = model4


t1 = time.process_time()
print("Calculating the predictions...")
y_predicted = model.predict(X_test)
print("--> Done")
tp4=time.process_time()-t1
print("Calculated in",tp4)

print("Calculating the accuracy of the model on the test data set...")
accuracy = 0
test_data_size = np.shape(y_test)[0] 
for k in range(test_data_size):
    if y_test[k]==y_predicted[k] : accuracy += 1
    
accuracy4 = 100*accuracy/test_data_size

print("--> Done")
print("The caccuraty is : ",accuracy4,"%")



#%%5

model = model5


t1 = time.process_time()
print("Calculating the predictions...")
y_predicted = model.predict(X_test)
print("--> Done")
tp5=time.process_time()-t1
print("Calculated in",tp5)

print("Calculating the accuracy of the model on the test data set...")
accuracy = 0
test_data_size = np.shape(y_test)[0] 
for k in range(test_data_size):
    if y_test[k]==y_predicted[k] : accuracy += 1
    
accuracy5 = 100*accuracy/test_data_size

print("--> Done")
print("The caccuraty is : ",accuracy5,"%")

#%%6

model = model6


t1 = time.process_time()
print("Calculating the predictions...")
y_predicted = model.predict(X_test)
print("--> Done")
tp6=time.process_time()-t1
print("Calculated in",tp6)

print("Calculating the accuracy of the model on the test data set...")
accuracy = 0
test_data_size = np.shape(y_test)[0] 
for k in range(test_data_size):
    if y_test[k]==y_predicted[k] : accuracy += 1
    
accuracy6 = 100*accuracy/test_data_size

print("--> Done")
print("The caccuraty is : ",accuracy6,"%")

#%% The prediction as binary images

# #load model
# model_path = ""
# model_name =  'model_SGDClassifier_random_1000.joblib' 
# model = load(model_path+model_name)

model = model5


def prediction(model, image_path, image_name):              #from an image, return a binary image of the model prediction
    X = np.zeros((768*768,3))
    f = imageio.imread(image_path+image_name)               #read image, f is like (ligne,column,color)                        
    for j in range(768) :
        for i in range(768) :
            X[768*j+i,0] = f[i,j,0]                         #concatenation for red values in the first X column
            X[768*j+i,1] = f[i,j,1]
            X[768*j+i,2] = f[i,j,2]
    y = model.predict(X)
    predicted_image = np.zeros((768,768))
    for i in range(768*768):
        predicted_image[ i%768, i//768] = y[i]
    return predicted_image


a=prediction(model,"../../../../data_projet_ship_detection/train_100/","00a52cd2a.jpg")
b=prediction(model,"../../../../data_projet_ship_detection/train_100/","00abc623a.jpg")
c=prediction(model,"../../../../data_projet_ship_detection/train_100/","00a9e2ec9.jpg")
d=prediction(model,"../../../../data_projet_ship_detection/train_100/","00b846e38.jpg")





plt.figure("1")

plt.subplot(1,2,1)
plt.imshow(a)
plt.subplot(1,2,2)
plt.imshow(img.imread("../../../../data_projet_ship_detection/train_100/00a52cd2a.jpg"))
plt.show()
plt.figure("2")

plt.subplot(1,2,1)
plt.imshow(b)
plt.subplot(1,2,2)
plt.imshow(img.imread("../../../../data_projet_ship_detection/train_100/00abc623a.jpg"))
plt.show()


plt.figure("3")

plt.subplot(1,2,1)
plt.imshow(c)
plt.subplot(1,2,2)
plt.imshow(img.imread("../../../../data_projet_ship_detection/train_100/00a9e2ec9.jpg"))
plt.show()


plt.figure("4")

plt.subplot(1,2,1)
plt.imshow(d)
plt.subplot(1,2,2)
plt.imshow(img.imread("../../../../data_projet_ship_detection/train_100/00b846e38.jpg"))
plt.show()

