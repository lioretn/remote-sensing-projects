# -*- coding: utf-8 -*-

from xlwt import Workbook
import numpy as np

# Different values of color
colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0), (255, 0, 255), (0, 255, 255), (255, 255, 255),
              (130, 0, 0), (0, 130, 0), (0, 0, 130), (130, 130, 0), (130, 0, 130), (0, 130, 130), (130, 130, 130)]
def image(boats):
    '''
        Function that return a "binary" image (different colors for each ship) from a list of boats (= list of sections),
    result of the encode_image function. This function was especially created in order to test and show different results
    and functions.
    '''
    im = np.zeros([768, 768, 3]) # Image creation
    i = 0 # To change the color when changing the boat
    
    for boat in boats:
        for s in boat: # Each sections of the boat is colorated
            im[s.start_pos[0], s.start_pos[1]:s.start_pos[1] + s.lenght, 0:3] = colors[i%len(colors)] # Coloring pixels
        
        i += 1
        
    return im.astype(np.uint8)


def areLinked(s1, s2):
    '''
        Function that tests if 2 sections are linked
    '''
    pos1, pos2 = s1.start_pos[1], s2.start_pos[1] # Position of the start of the section
    ext1, ext2 = pos1 + s1.lenght, pos2 + s2.lenght # Position of the end of the section
    
    # Case one extremity is linked to the upper section, or the upper section is completly linked to the lower section
    if (pos1 >= pos2 and pos1 <= ext2) or (ext1 >= pos2 and ext1 <= ext2) or (pos1 < pos2 and ext1 > ext2):
        return True # The 2 sections are effectively linked
    return False


def formBoat(s, boat):
    '''
        Recursive function that put all the linked sections in a list (boat) which represents a ship.
        When there is too much noise, there are problems of recursive depht with this function, that's why there's a
    try-except stuff in encode_image.
    '''
    boat.append(s)
    s.isBoated = True
    
    for sec in s.sections:
        if not sec.isBoated:
            formBoat(sec, boat) # Recursive
   
    
class Section:
    '''
        This class represent a data as considered in the Airbus excels : a start_pos position is the first pixel of the
    section, and lenght is the lenght of section. For exemple : start_pos = (12, 24) and lenght = 5 would mean that 
    (12, 24), (12, 25), (12, 26), (12, 27) and (12, 28) are "ship" pixels.
    '''
    instances = [] # All Section object created
    def __init__(self, start_pos, lenght):
        self.start_pos = start_pos
        self.lenght = lenght
        self.sections = [] # The sections linked to this one
        self.isBoated = False # When generating the boats, in order to know if this sections had already been treated 
   
        
def encode_image(bimage, minimum_size = 100):
    '''
        This function encodes the image bimage by differentiating the different boats on the binary image bimage (also reducing the noise at
    the same time).
        minimum_size corresponds to the minimum amount of linked pixels that are requires to consider this as a ship
    (it's for unoising).
    '''
    sections = [[] for i in range(768)] # The sections of each line
    
    # Locating the different continuous sections
    start = False
    start_pos = (0, 0)
    c = 1
    for i in range(768):
        for j in range(768):
            if bimage[i, j, 0] == 255: # Ship pixel
                if not start: # We havn't started a section
                    c = 1 # There's only 1 pixel for now...
                    start_pos = (i, j) # ...at the start position
                    start = True # Now, we are in a section
                elif i == start_pos[0]: # We're still on the same line
                    c += 1 # The lenght of the section is just increasing
                else: # We've passed to the next line,
                    s = Section(start_pos, c) # so we end the section
                    sections[start_pos[0]].append(s) # to the right line
                    Section.instances.append(s) 
                    c = 1 # and start a new one
                    start_pos = (i, j)
                    
            elif start: # The section is over (not a ship pixel)
                s = Section(start_pos, c)
                sections[start_pos[0]].append(s)
                Section.instances.append(s) 
                start = False # We're not in a section anymore
                
    # Linking the different sections
    for i in range(1, len(sections)):
        # We only look at two adjacent lines
        for s1 in sections[i]:
            for s2 in sections[i-1]:
                if areLinked(s1, s2):
                    s1.sections.append(s2) # We link both the sections
                    s2.sections.append(s1)
                    
    # Forming the boats
    boats = []
    for s in Section.instances:
        if not s.isBoated: # If the section isn't already in a ship
            boat = []
            # The try...except stuff is only here to dodge the "maximum recursive depth reached" that occurs when using
            # this algorithm on a very noisy image (that isn't supposed to happen...)
            try:
                formBoat(s, boat) # It completes the boat list with all the sections linked to s (directly or not)
            except:
                pass
            
            # Number of ship pixels in this group (boat)
            ships_number = 0
            for sec in boat:
                ships_number += sec.lenght
                
            if not ships_number < minimum_size: # If the amount of ship isn't too low (= noise)
                boats.append(boat)  
            
    return boats
     

def encode_boats(boats):
    '''
        A little function that put the datas in the right form (required by Airbus). It takes the boats list in entry, the one
    returned by encode_image, and return encoded_pixels as required by write_in_excel function.
    '''
    # Encoding the boats list
    encoded_pixels = []
    for boat in boats:
        line = "" # 1 line = 1 boat
        for s in boat:
            pos = s.start_pos[0]*768 + s.start_pos[1]
            line += str(pos) + " " + str(s.lenght) + " " # Same form as required by Airbus
        encoded_pixels.append(line)
        
    return encoded_pixels

          
def write_in_excel(image_id, encoded_pixels, path):
    '''
        Function that writes the results of a test on a method for ship detection in an excel file as asked by
    Airbus for his challenge.
        Parameters :
            - image_names : all the lines in the column ImageId
            - encoded_pixels : all the lines in the column EncodedPixels
            - path : where we want to put our excel file
    '''
    # Opening the file
    excel = Workbook()
    sheet = excel.add_sheet("OCB")
    # Header
    sheet.write(0, 0, "ImageId")
    sheet.write(0, 1 ,"EncodedPixels")
    # Write every datas
    for i in range(len(image_id)):
        sheet.write(i+1, 0, image_id[i])
        sheet.write(i+1, 1, encoded_pixels[i])
    # Saving the file 
    excel.save(path)
    