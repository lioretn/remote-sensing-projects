# -*- coding: utf-8 -*-

#############################################################################
# Update the excel  in order to consider only the images we use #
#############################################################################

from xlwt import Workbook # For writing in the excel

csv = open("../../../data/train_ship_segmentations_original.csv", 'r') # Original file

# Creation of the new excel
new_excell = Workbook()
sheet = new_excell.add_sheet("OCB")
    
csv_lines = csv.readlines()
# Header
csv_lines[0] = csv_lines[0].split(",")
sheet.write(0, 0, csv_lines[0][0]) # ImageID
sheet.write(0, 1, csv_lines[0][1][:-1]) # EncodedPixels
image_names = []

size = 1000 # Number of images in the considered dataset (test or train) = number of files in the folder
c_images = 0 ; c = 0 ; i = 1
filename = ""
# We get the names of the images and we add the corresponding line from the original excel lines to the new excel
while c_images < size or csv_lines[i].split(",")[0] in image_names:
   filename = csv_lines[i].split(",")
   path = "../../../data/test/"  + filename[0] # The theorical path of the image (we can put test or train)
   
   # If the file exists, we add the coresponding line from the original excel to the new one
   try: 
       with open(path, 'r') as file: # Trying opening the image
           sheet.write(c+1, 0, filename[0])
           sheet.write(c+1, 1, filename[1][:-1])
           if filename[0] not in image_names:
               image_names.append(filename[0])
               c_images += 1
           c += 1
               
           print("Avancement :", c_images/size*100, "%") # Pour voir où on en est...
           
   except: # Else, we do nothing
       pass
   
   i += 1

# Close the original excel and save the new one               
csv.close()
new_excell.save("../../../data/test/tesst_ship_segmentations.xls")  # Save (we can put test or train)      
    